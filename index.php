<?php
require_once './vendor/autoload.php';

/*/
$loader = new \Twig\Loader\ArrayLoader([
    'header' => './header.php',
    'index' => 'Hello {{ name }}!',
]);
$twig = new \Twig\Environment($loader);
/*/

$loader = new \Twig\Loader\FilesystemLoader('./view');
$twig = new \Twig\Environment($loader, [
    //'cache' => './cache',
]);

$template = $twig->load('index.html');

$param["name"] = "World";

$json = file_get_contents('./assets/json/extension.json');
$param["ext"] = json_decode($json);
$divider = round(count($param["ext"])/4);
$count = $divider;
$ctr = 1;
foreach ($param["ext"] as $key=>$value) {
	if ($key >= $count) {
		$count += $divider;
		$ctr++;
	}
	$param["ext".$ctr][] = $value;
}

$json = file_get_contents('./assets/json/package.json');
$param["package"] = json_decode($json);
foreach ($param["package"] as $key => $value) {
	$price = $param["package"][$key]->price->discounted;	
	$text = "Rp <strong>" . floor($price / 1000) . "</strong>.<span>" . ($price % 1000) . "</span>/ bln";
	$param["package"][$key]->price->discounted_text = $text;
}

echo $template->render($param);
?>
					